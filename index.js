const button = document.getElementById("btn");
const inputnum = document.getElementById("input");
const container = document.getElementById("container");

function changeproduct(num) {
    fetch(`https://fakestoreapi.com/products/${num}`)
        .then(response => response.json()) // Corrected syntax error here
        .then(data => {
            console.log(data);
            const tit = data.title;
            const imageSrc = data.image;
            const price = data.price;
            const des = data.description;

            const span = document.createElement("span");
            span.classList.add("pric");
            span.innerHTML = tit;
           

            const img = document.createElement("img");
            img.classList.add("product1");
            img.src = imageSrc;
           

            const span2 = document.createElement("span2");
            span2.classList.add("pric");
            span2.innerHTML = "$" + price;
            

            const span3 = document.createElement("span3");
            span3.classList.add("disc");
            span3.innerHTML = "Description:" + "&nbsp;" + des;

            const div = document.createElement("div")
            div.classList.add("content")
            div.appendChild(span)
            div.appendChild(img);
            div.appendChild(span2);
            div.appendChild(span3);


            container.appendChild(div);
        })
        .catch(error => {
            console.log("Error fetching data:", error);
        });
}

button.addEventListener("click", function(){
    console.log("button clicked");
    const num = inputnum.value;
    changeproduct(num);
});



inputnum.addEventListener("keydown", function(){
    console.log("keydown worked");
    
});